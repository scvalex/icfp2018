all:
	cargo watch

solve: debug
	ls problemsF/FA*_tgt.mdl | parallel --will-cite --bar 'target/debug/nmms assemble {}'

debug:
	cargo build

release:
	cargo build --release

clean:
	git clean -dxf

clippy:
	cargo +nightly clippy
