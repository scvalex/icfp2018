/// Nanobot commands

#[allow(dead_code)]
#[derive(Debug)]
pub enum Lld {
    Dx(i8),
    Dy(i8),
    Dz(i8),
}

impl Lld {
    pub fn encode(&self) -> (u8, u8) {
        match self {
            Lld::Dx(i) => (0b01, (i + 15) as u8),
            Lld::Dy(i) => (0b10, (i + 15) as u8),
            Lld::Dz(i) => (0b11, (i + 15) as u8),
        }
    }
}

#[allow(dead_code)]
#[derive(Debug)]
pub enum Sld {
    Dx(i8),
    Dy(i8),
    Dz(i8),
}

impl Sld {
    pub fn encode(&self) -> (u8, u8) {
        match self {
            Sld::Dx(i) => (0b01, (i + 5) as u8),
            Sld::Dy(i) => (0b10, (i + 5) as u8),
            Sld::Dz(i) => (0b11, (i + 5) as u8),
        }
    }
}

#[allow(dead_code)]
#[derive(Debug)]
pub struct Nd {
    pub dx: i8,
    pub dy: i8,
    pub dz: i8,
}

impl Nd {
    fn encode(&self) -> u8 {
        ((self.dx + 1) * 9 + (self.dy + 1) * 3 + (self.dz + 1)) as u8
    }
}

#[allow(dead_code)]
#[derive(Debug)]
pub struct Fd {
    pub dx: i8,
    pub dy: i8,
    pub dz: i8,
}

#[allow(dead_code)]
#[derive(Debug)]
pub enum Cmd {
    Halt,
    Wait,
    Flip,
    SMove(Lld),
    LMove { sld1: Sld, sld2: Sld },
    Fission { nd: Nd, m: u8 },
    Fill(Nd),
    Void(Nd),
    FusionP(Nd),
    FusionS(Nd),
    GFill { nd: Nd, fd: Fd },
    GVoid { nd: Nd, fd: Fd },
}

impl Cmd {
    pub fn write_to(&self, buf: &mut Vec<u8>) {
        match self {
            Cmd::Halt => buf.push(0b1111_1111),
            Cmd::Wait => buf.push(0b1111_1110),
            Cmd::Flip => buf.push(0b1111_1101),
            Cmd::SMove(lld) => {
                let (a, i) = lld.encode();
                buf.push(0b0000_0100 | (a << 4));
                buf.push(i);
            }
            Cmd::LMove { sld1, sld2 } => {
                let (a1, i1) = sld1.encode();
                let (a2, i2) = sld2.encode();
                buf.push(0b0000_1100 | (a2 << 6) | (a1 << 4));
                buf.push((i2 << 4) | i1);
            }
            Cmd::Fission { nd, m } => {
                buf.push(0b0000_0101 | nd.encode());
                buf.push(*m);
            }
            Cmd::Fill(nd) => buf.push(0b0000_0011 | (nd.encode() << 3)),
            Cmd::Void(nd) => buf.push(0b0000_0010 | (nd.encode() << 3)),
            Cmd::FusionP(nd) => buf.push(0b0000_0111 | (nd.encode() << 3)),
            Cmd::FusionS(nd) => buf.push(0b0000_0110 | (nd.encode() << 3)),
            Cmd::GFill { nd, fd } => {
                buf.push(0b0000_0001 | (nd.encode() << 3));
                buf.push((fd.dx + 30) as u8);
                buf.push((fd.dy + 30) as u8);
                buf.push((fd.dz + 30) as u8);
            }
            Cmd::GVoid { nd, fd } => {
                buf.push(nd.encode() << 3);
                buf.push((fd.dx + 30) as u8);
                buf.push((fd.dy + 30) as u8);
                buf.push((fd.dz + 30) as u8);
            }
        }
    }
}
