#[macro_use]
extern crate failure;

use std::fs::File;
use std::io::{Read, Write};

mod cmd;
mod model;
mod simple_trace;

use cmd::*;
use model::*;

type Result<T> = std::result::Result<T, failure::Error>;

pub fn parse(file: &str) -> Result<()> {
    let model = load_model(file)?;
    println!("{:?}", model);
    Ok(())
}

pub fn assemble(file: &str) -> Result<()> {
    if !file.ends_with("_tgt.mdl") {
        bail!("model file name does not end with '_tgt.mdl': {}", file)
    }
    let model = load_model(file)?;
    let file = format!("{}.nbt", &file[..file.len() - 8]);
    let trace = simple_trace::trace(&model);
    write_trace(&file, trace)?;
    Ok(())
}

pub fn load_model(file: &str) -> Result<Model> {
    if !file.ends_with(".mdl") {
        bail!("model file name does not end with '.mdl': {}", file)
    }
    let mut f = File::open(file)?;
    let mut buf = vec![];
    f.read_to_end(&mut buf)?;
    Ok(Model::new(buf[0], buf.split_off(1)))
}

fn write_trace(file: &str, trace: Vec<Cmd>) -> Result<()> {
    if !file.ends_with(".nbt") {
        bail!("trace file name does not end with '.mdl': {}", file)
    }
    let mut buf = vec![];
    for c in trace {
        c.write_to(&mut buf);
    }
    println!("Writing trace to file {}: {} bytes", file, buf.len());
    let mut f = File::create(file)?;
    f.write_all(&buf)?;
    Ok(())
}
