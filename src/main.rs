extern crate failure;
extern crate nmms;
#[macro_use]
extern crate structopt;

use nmms::*;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
#[structopt(name = "nmms")]
enum Args {
    #[structopt(name = "parse")]
    Parse { file: String },
    #[structopt(name = "assemble")]
    Assemble { file: String },
}

fn main() -> Result<(), failure::Error> {
    let args = Args::from_args();
    match args {
        Args::Parse { file } => parse(&file),
        Args::Assemble { file } => assemble(&file),
    }
}
