#[derive(Debug)]
pub struct Model {
    resolution: u8,
    data: Vec<u8>,
}

impl Model {
    pub fn new(resolution: u8, data: Vec<u8>) -> Model {
        Model { resolution, data }
    }

    pub fn resolution(&self) -> u8 {
        self.resolution
    }

    pub fn is_full(&self, x: u8, y: u8, z: u8) -> bool {
        let r = u32::from(self.resolution);
        let idx = (u32::from(x) * r * r + u32::from(y) * r + u32::from(z)) as usize;
        0 != self.data[idx / 8] & (1 << (idx % 8))
    }
}
