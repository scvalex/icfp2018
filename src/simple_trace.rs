use cmd::*;
use model::Model;

use Cmd::*;

pub fn trace(model: &Model) -> Vec<Cmd> {
    let mut trace = vec![Flip, SMove(Lld::Dy(1))];
    let max_steps = model.resolution() - 1;
    for y in 1..max_steps {
        for z in 0..max_steps {
            if z % 2 == 0 {
                for x in 0..max_steps {
                    if model.is_full(x, y - 1, z) {
                        trace.push(Fill(Nd {
                            dx: 0,
                            dy: -1,
                            dz: 0,
                        }))
                    }
                    trace.push(SMove(Lld::Dx(1)))
                }
                if z == max_steps - 1 {
                    for _ in 0..max_steps {
                        trace.push(SMove(Lld::Dx(-1)))
                    }
                }
            } else {
                for x in (0..max_steps).rev() {
                    trace.push(SMove(Lld::Dx(-1)));
                    if model.is_full(x, y - 1, z) {
                        trace.push(Fill(Nd {
                            dx: 0,
                            dy: -1,
                            dz: 0,
                        }))
                    }
                }
            }
            trace.push(SMove(Lld::Dz(1)))
        }
        for _ in 0..max_steps {
            trace.push(SMove(Lld::Dz(-1)))
        }
        trace.push(SMove(Lld::Dy(1)))
    }
    for _ in 0..max_steps {
        trace.push(SMove(Lld::Dy(-1)))
    }
    trace.push(Flip);
    trace.push(Halt);
    trace
}
